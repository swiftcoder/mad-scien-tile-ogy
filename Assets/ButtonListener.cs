﻿using UnityEngine;
using System.Collections;

public class ButtonListener : MonoBehaviour {
	public Canvas inGameCanvas;
	public Canvas pausedCanvas;
	public Canvas levelCompleteCanvas;
	public Canvas levelFailedCanvas;

	private Canvas[] canvases = new Canvas[4];
	private int currentCanvas = 0;

	public void nextCanvas() {
		print ("next canvas");
		canvases [currentCanvas].enabled = false;
		currentCanvas = (currentCanvas + 1) % canvases.Length;
		canvases [currentCanvas].enabled = true;
	}

	public void quitToMenu() {
		Application.LoadLevel ("intro");
	}

	// Use this for initialization
	void Start () {
		canvases [0] = inGameCanvas;
		canvases [1] = pausedCanvas;
		canvases [2] = levelCompleteCanvas;
		canvases [3] = levelFailedCanvas;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
