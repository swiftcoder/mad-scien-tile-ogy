﻿using UnityEngine;
using System.Collections;

public class RandomCreepClip : MonoBehaviour {
	public Animator animator;
	private static bool lastClipWasEye = false;
	private static bool alternateEyeClip = false;

	public void nextRandomClip() {
		float rVal = Random.Range (0f, 1f);
		bool canPlayEyeClip = !lastClipWasEye;
		lastClipWasEye = false;

		if (rVal < 0.1) {
			animator.Play ("idle");
		} else if (rVal < 0.2) {
			animator.Play ("idle2");
		} else if (rVal < 0.3) {
			animator.Play ("idle3");
		} else if (rVal < 0.5) {
			animator.Play ("idle-bubbles");
		} else if (rVal < 0.6) {
			animator.Play ("idle-lumps");
		} else if (rVal < 0.7) {
			animator.Play ("idle-bumps");
		} else if (canPlayEyeClip) {
			alternateEyeClip = !alternateEyeClip;
			if(alternateEyeClip) {
				animator.Play ("idle-eyes");
			}
			else {
				animator.Play ("idle-eyes2");
			}
			lastClipWasEye = true;
		} else {
			animator.Play ("idle");
		}
	}

	// Use this for initialization
	void Start () {
		nextRandomClip ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
