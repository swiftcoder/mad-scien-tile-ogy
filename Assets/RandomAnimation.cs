﻿using UnityEngine;
using System.Collections;

public class RandomAnimation : MonoBehaviour {
	public float minSpeed = 0.5f;
	public float maxSpeed = 1.0f;

	// Use this for initialization
	void Start () {
		Animator animator = GetComponent<Animator> ();
		if (animator) {
			animator.speed = Random.Range(minSpeed,maxSpeed);
		}
	}
	
}
