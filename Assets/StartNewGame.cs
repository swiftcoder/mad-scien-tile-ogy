﻿using UnityEngine;
using System.Collections;

public class StartNewGame : MonoBehaviour {

	AsyncOperation status;

	public void Start() {
		status = Application.LoadLevelAsync("main");
		status.allowSceneActivation = false;
	}

	public void StartGame() {
		status.allowSceneActivation = true;
	}
}
