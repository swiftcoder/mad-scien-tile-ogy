﻿using UnityEngine;
using System.Collections;

public class FlashAndDie : MonoBehaviour {
	
	private bool flashed = false;
	private SpriteRenderer spriteRenderer;
	private Animator animator;
	public Sprite original, alternate;

	void Start() {
		spriteRenderer = GetComponent<SpriteRenderer>();
		animator = GetComponent<Animator>();
	}

	void Update() {
	}

	public void flashAndDie() {
		Vector3 pos = transform.localPosition;
		pos.z = -3;
		transform.localPosition = pos;

		animator.Stop();

		InvokeRepeating("flash", 0.1f, 0.1f);
		print("flash and die!");
		
		Destroy(gameObject, 2f);
	}

	private void flash() {
		print("flashing!!!");
		if (flashed) {
			spriteRenderer.sprite = original;
		} else {
			spriteRenderer.sprite = alternate;
		}
		flashed = !flashed;
	}
}
