﻿using UnityEngine;
using System.Collections;

public class LevelCompleteController : CanvasController {
	public ScoreCounter scoreCounter;
	public GameScore gameScore;

	public override void Show(bool animated = true) {
		base.Show (animated);
		scoreCounter.setScore (gameScore.getScore (), true);
	}

	public override void Hide(bool animated = true) {
		base.Hide (animated);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
