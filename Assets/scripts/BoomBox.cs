﻿
using UnityEngine;
using System.Collections;

public class BoomBox : MonoBehaviour {

	public AudioClip[] tileClearSounds;
	public AudioClip[] slurpSounds;
	public AudioClip popSound;
	public AudioClip playerSuperSound;
	public AudioClip creepSuperSound;

	public AudioClip levelWinMusic;
	public AudioClip levelLoseMusic;
	
	AudioSource source;

	public AudioSource titleThemeLoop;
	
	void Start() {
		source = GetComponent<AudioSource>();
	}

	void Update() {
	
	}

	public void onTileCleared(int count, int chain) {
		if (chain < tileClearSounds.Length) {
			source.PlayOneShot(tileClearSounds[chain]);
		}
	}

	public void onTastyTileConsumed() {
		source.PlayOneShot(slurpSounds[Random.Range(0, slurpSounds.Length)]);
	}

	public void onCreepInfest() {
		source.PlayOneShot(popSound, 2.0f);
	}

	public void onLevelStart() {
		if (titleThemeLoop != null) {
			titleThemeLoop.Stop();
		}

		if (source != null) {
			source.Stop();
			source.Play();
		}
	}

	public void onLevelWin() {
		source.Stop();
		source.PlayOneShot(levelWinMusic);

		titleThemeLoop.PlayDelayed(levelWinMusic.length);
	}

	public void onLevelFail() {
		source.Stop();
		source.PlayOneShot(levelLoseMusic);

		titleThemeLoop.PlayDelayed(levelWinMusic.length);
	}
	
	public void onPlayerSuper() {
		source.PlayOneShot(playerSuperSound, 1.5f);
	}

	public void onCreepSuper() {
		source.PlayOneShot(creepSuperSound, 1.5f);
	}
}
