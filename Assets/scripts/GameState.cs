﻿using UnityEngine;
using System.Collections;

public class GameState : MonoBehaviour {

	public CanvasController levelCompleteController;
	public CanvasController levelFailedController;
	public CanvasController levelPausedCanvas;
	public GameBoard gameBoard;
	public GameScore gameScore;

	private CanvasController currentCanvas = null;

	public void OnQuitToMenu() {
		Application.LoadLevel ("intro");
	}

	public void OnTimeExpired() {
		gameBoard.doPause();
		if (gameBoard.didCreepWin()) {
			switchToCanvas(levelFailedController);
			gameBoard.GetComponent<BoomBox>().onLevelFail();
		} else {
			switchToCanvas(levelCompleteController);
			gameBoard.GetComponent<BoomBox>().onLevelWin();
		}
	}

	public void OnNextLevel() {
		if (currentCanvas) {
			currentCanvas.Hide ();
		}

		gameBoard.doStartLevel(true);
	}

	public void OnRetryLevel() {
		if (currentCanvas) {
			currentCanvas.Hide ();
		}

		gameBoard.doStartLevel(false);
	}

	public void OnPause() {
		switchToCanvas(levelPausedCanvas);

		gameBoard.doPause();
	}

	public void OnResume() {
		if (currentCanvas) {
			currentCanvas.Hide ();
		}

		gameBoard.doResume();
	}

	public void OnFail() {
		gameBoard.doPause();
		switchToCanvas(levelFailedController);
		gameBoard.GetComponent<BoomBox>().onLevelFail();
	}

	private void switchToCanvas(CanvasController canvas) {
		if (currentCanvas) {
			currentCanvas.Hide();
		}

		canvas.Show ();
		currentCanvas = canvas;
	}
}
