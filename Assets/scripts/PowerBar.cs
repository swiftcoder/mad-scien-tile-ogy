﻿using UnityEngine;
using UnityEngine.UI;
using System;

using System.Collections;

public class PowerBar : BaseBar {

	protected override void onMeterDrained() {
		if (gameBoard != null) {
			gameBoard.killAllCreep();
		}
	}

	protected override void onMeterFull() {
		print("Your meter overfloweth with science!");
		if (gameBoard != null) {
			gameBoard.GetComponent<BoomBox>().onPlayerSuper();
		}
	}
}
