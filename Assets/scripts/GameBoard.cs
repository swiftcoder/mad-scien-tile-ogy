﻿
using UnityEngine;
using System.Collections.Generic;
using System;

public class GameBoard : MonoBehaviour {

	private readonly int[] scoreLevels = {100, 200, 1000, 10000};

	public GameObject[] gemPrefabs;
	public GameObject[] creepPrefabs;
	public GameObject[] scorePrefabs;
	public GameObject explosionPrefab;
	public int size = 16;

	public GameScore gameScore;
	public GameTimer gameTimer;

	public GameState gameState;
	
	private bool gameHasStarted;

	private GameObject[] board;

	private int mouseX, mouseY;
	private float rawX, rawY;
	private GameObject drag = null;

	private List<Animation> animations = new List<Animation>();
	private float duration = 0;
	
	private float creepDuration = 0;

	private int chain = 0;

	private Dictionary<int, int> accumulateScores = new Dictionary<int, int>();

	private bool isPaused = true;
	private float creepSuperTimer = -1;

	private class Swap {
		public int x1, y1, x2, y2;

		public Swap(int x1, int y1, int x2, int y2) {
			this.x1 = x1;
			this.y1 = y1;
			this.x2 = x2;
			this.y2 = y2;
		}
	}

	private List<Swap> swapQueue = new List<Swap>();
	
	private interface Animation {
		void update(float deltaTime);
	}
	
	private abstract class BaseAnimation : Animation {
		public GameObject target;
		public float start, end;
		private float duration, delay, progress;

		public BaseAnimation(GameObject target, float start, float end, float duration, float delay) {
			this.target = target;
			this.start = start;
			this.end = end;
			this.duration = duration;
			this.delay = delay;
			this.progress = 0;
			this.target.GetComponent<TileState>().isAnimating = true;
		}

		public void update(float deltaTime) {
			progress += deltaTime;
			if (progress > duration) {
				progress = duration;
				this.target.GetComponent<TileState>().isAnimating = false;
			}

			float delta = Mathf.Max(0, progress - delay) / (duration - delay);
			doUpdate(delta);
		}

		protected abstract void doUpdate(float proress);
	}

	private class VerticalSlideAnimation : BaseAnimation {
		public VerticalSlideAnimation(GameObject target, float start, float end, float duration, float delay = 0) : base(target, start, end, duration, delay) {}
		
		protected override void doUpdate(float progress) {
			Vector3 pos = getLocalPosition(target);
			pos.y = start + (end - start) * progress;
			setLocalPosition(target, pos.x, pos.y, pos.z);
		}
	}

	private class ScaleAnimation : BaseAnimation {
		public ScaleAnimation(GameObject target, float start, float end, float duration, float delay = 0) : base(target, start, end, duration, delay) {}
		
		protected override void doUpdate(float progress) {
			float actual = start + (end - start) * progress;
			Vector3 scale = new Vector3(actual, actual, actual);
			target.transform.localScale = scale;
		}
	}
	
	private void setTile(GameObject tile, int x, int y, bool setPosition = true) {
		if (tile != null && setPosition) {
			setLocalPosition(tile, x, y, 0);
		}
		board[x * size + y] = tile;
	}

	private GameObject getTile(int x, int y) {
		return board[x * size + y];
	}

	public bool didCreepWin() {
		for (int i = 0; i < size; ++i) {
			for (int j = 0; j < size; ++j) {
				if (getTile(i, j) != null && !getTile(i, j).GetComponent<TileState>().isCreep) {
					return false;
				}
			}
		}

		return true;
	}

	void Start() {
		board = new GameObject[size*size];

		doStartLevel(false);
	}

	public void doPause() {
		isPaused = true;

		if (gameTimer != null) {
			gameTimer.doPause();
		}
	}

	public void doResume() {
		isPaused = false;

		if (gameTimer != null) {
			gameTimer.doResume();
		}
	}

	public void doStartLevel(bool next) {
		isPaused = false;

		GetComponent<BoomBox>().onLevelStart();

		if (next) {
			gameTimer.durationInSeconds = Math.Min(gameTimer.durationInSeconds + 30, 300);
		}

		animations.Clear();

		for (int i = 0; i < size; ++i) {
			for (int j = 0; j < size; ++j) {
				GameObject t = getTile(i, j);
				if (t != null) {
					Destroy(t);
				}

				t = (GameObject)Instantiate(gemPrefabs[UnityEngine.Random.Range(0, gemPrefabs.Length)]);
				t.transform.parent = gameObject.transform;
				setTile(t, i, j);
			}
		}
		
		gameHasStarted = false;
		doExplosions();
	}

	void Update() {
		if (isPaused)
			return;

		if (duration > 0) {
			print("animating");
			foreach (Animation a in animations) {
				a.update(Time.deltaTime);
			}

			duration -= Time.deltaTime;
			if (duration <= 0) {
				print("done animating");
				animations.Clear();
				doExplosions();
			}
		} else {
			doSwaps();

			if (gameHasStarted) {
				creepDuration -= Time.deltaTime;
				if (creepDuration < 0) {
					doExpandCreep();
					if (creepSuperTimer >= 0) {
						creepDuration = 0.2f;
					} else {
						creepDuration = 1.0f;
					}
				}

				if (creepSuperTimer >= 0) {
					creepSuperTimer -= Time.deltaTime;
				}
			}
		}
	}

	private void doSwaps() {
		if (swapQueue.Count > 0) {
			while (swapQueue.Count > 0) {
				Swap s = swapQueue[0];
				swapQueue.RemoveAt(0);

				GameObject t1 = getTile(s.x1, s.y1);
				GameObject t2 = getTile(s.x2, s.y2);

				if (t1.GetComponent<TileState>().uCantTouchThis() || t2.GetComponent<TileState>().uCantTouchThis())
					continue;

				setTile(t1, s.x2, s.y2);
				setTile(t2, s.x1, s.y1);

				if (findExplosions() == 0) {
					setTile(t2, s.x2, s.y2);
					setTile(t1, s.x1, s.y1);
				}

				doExplosions();
			}
		}
	}
	
	private void getMouseCoords(out int x, out int y, out float rawX, out float rawY) {
		Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		rawX = position.x;
		rawY = position.y;

		// Don't ask about this math. Grr.
		x = Mathf.RoundToInt(position.x - transform.position.x - 0.5f);
		y = Mathf.RoundToInt(position.y - transform.position.y - 0.5f);
	}
	
	void OnMouseDown() {
		if (isPaused || !gameHasStarted)
			return;

		getMouseCoords(out mouseX, out mouseY, out rawX, out rawY);
		if (mouseX >= 0 && mouseX < size && mouseY >= 0 && mouseY < size) {
			drag = getTile(mouseX, mouseY);

			// Don't pickup tiles unless you are allowed to
			if (drag != null && drag.GetComponent<TileState>().uCantTouchThis()) {
				drag = null;
			}

			// Don't pickup a tile that is in the middle of an animation
			if (drag != null && drag.GetComponent<TileState>().isAnimating) {
				drag = null;
			}

			if (drag != null) {
				drag.transform.localScale = new Vector3(1.1f, 1.1f, 1.1f);
			}
		}
	}

	void OnMouseDrag() {
		if (drag == null)
			return;

		int x, y;
		float rx, ry;
		getMouseCoords(out x, out y, out rx, out ry);

		float dx = rx - rawX;
		float dy = ry - rawY;

		setLocalPosition(drag, mouseX + dx, mouseY + dy, -1);
	}

	void OnMouseUp() {
		if (drag == null)
			return;

		drag.transform.localScale = new Vector3(1, 1, 1);

		int x, y;
		float rx, ry;
		getMouseCoords(out x, out y, out rx, out ry);

		int dx = Math.Abs(x - mouseX), dy = Math.Abs(y - mouseY);

		if (dx > dy) {
			dy = 0;
			y = mouseY;
		} else if (dy > dx) {
			dx = 0;
			x = mouseX;
		}

		if (dx != 0) {
			x = mouseX + (x - mouseX) / dx;
		}
		if (dy != 0) {
			y = mouseY + (y - mouseY) / dy;
		}

		print(String.Format("{0}, {1}", dx, dy));

		if (x >= 0 && x < size && y >= 0 && y < size && (dx == 0 || dy == 0)) {
			swapQueue.Add(new Swap(mouseX, mouseY, x, y));
		}

		setTile(drag, mouseX, mouseY);
	}
	
	private int findExplosions(bool[] destroy = null, bool recordScores = false) {
		print("finding explosions");

		if (destroy == null) {
			destroy = new bool[size*size];
		}

		// first scan columns
		for (int i = 0; i < size; ++i) {
			GameObject last = getTile(i, 0);
			int runLength = 1;
			for (int j = 1; j <= size; j++) {
				if (runLength >= 3) {
					for (int k = 1; k <= runLength; k++) {
						destroy[i * size + (j - k)] = true;
					}
				}

				if (j < size) {
					GameObject current = getTile(i, j);
					if (current != null && last != null &&
					    current.GetComponent<TileState>().matches(last.GetComponent<TileState>())) {
						++runLength;
					} else {
						if (runLength >= 3 && recordScores) {
							onScoreEvent(runLength, i, j - runLength/2);
						}
						runLength = 1;
					}
					last = current;
				}
			}
		}

		// now scan rows
		for (int j = 0; j < size; j++) {
			GameObject last = getTile(0, j);
			int runLength = 1;
			for (int i = 1; i <= size; ++i) {
				if (runLength >= 3) {
					for (int k = 1; k <= runLength; k++) {
						destroy[(i - k) * size + j] = true;
					}
				}
				
				if (i < size) {
					GameObject current = getTile(i, j);
					if (current != null && last != null &&
					    current.GetComponent<TileState>().matches(last.GetComponent<TileState>())) {
						++runLength;
					} else {
						if (runLength >= 3 && recordScores) {
							onScoreEvent(runLength, i - runLength/2, j);
						}
						runLength = 1;
					}
					last = current;
				}
			}
		}

		int explosions = 0;
		
		// count all the tiles that will be exploded
		for (int i = 0; i < size; ++i) {
			for (int j = 0; j < size; ++j) {
				if (destroy[i * size + j]) {
					++explosions;
				}
			}
		}

		return explosions;
	}

	private void doExplosions() {
		bool[] destroy = new bool[size*size];

		int explosions = findExplosions(destroy, true);

		// wipe out all the tiles matching the destroyed locations
		for (int i = 0; i < size; ++i) {
			for (int j = 0; j < size; ++j) {
				if (destroy[i * size + j]) {
					GameObject t = getTile(i, j);
					setTile(null, i, j);
					Destroy(t);
					killCreep(i, j);
					spawnExposion(i, j);
				}
			}
		}

		if (explosions > 0) {
			if (gameHasStarted) {
				GetComponent<BoomBox>().onTileCleared(explosions, chain);
			}
			++chain;
			submitScores();
			doGravity();
		} else {
			if (!gameHasStarted) {
				gameHasStarted = true;
				if (gameTimer != null) {
					gameTimer.doBegin();
				}
			}
			chain = 0;
		}
	}

	private void spawnExposion(int x, int y) {
		if (gameHasStarted) {
			print("spawning explosion");
			GameObject t = (GameObject)Instantiate(explosionPrefab);
			t.transform.parent = gameObject.transform;
			setLocalPosition(t, x, y, -1.5f);
		}
	}

	public void killAllCreep() {
		print("killing all creep!");
		for (int i = 0; i < size; ++i) {
			for (int j = 0; j < size; ++j) {
				GameObject t = getTile(i, j);
				if (t != null && t.GetComponent<TileState>().isCreep) {
					setTile(null, i, j);
					t.GetComponent<FlashAndDie>().flashAndDie();
//					Destroy(t);
				}
			}
		}

		Invoke("doGravity", 1f);
		//doGravity();
	}

	private void doGravity() {
		for (int j = 1; j < size; ++j) {
			for (int i = 0; i < size; ++i) {
				GameObject t = getTile(i, j);
				if (t != null) {
					for (int k = 0; k < j; k++) {
						if (getTile(i, k) == null) {
							animations.Add(new VerticalSlideAnimation(t, j, k, 0.125f));
							setTile(t, i, k, false);
							setTile(null, i, j);
							break;
						}
					}
				}
			}
		}
		duration = 0.25f;

		doSpawn();
	}

	private void doSpawn() {
		for (int i = 0; i < size; ++i) {
			for (int j = 0; j < size; ++j) {
				if (getTile(i, j) == null) {
					spawnPrefab(gemPrefabs[UnityEngine.Random.Range(0, gemPrefabs.Length)], i, j);
				}
			}
		}
	}

	private void killCreep(int x, int y) {
		// first horizontal
		for (int i = Math.Max(0, x-1); i < Math.Min(size, x+2); i++) {
			GameObject t = getTile(i, y);
			if (t != null && t.GetComponent<TileState>().isCreep) {
				setTile(null, i, y);
				Destroy(t);
			}
		}

		// then vertical
		for (int j = Math.Max(0, y-1); j < Math.Min(size, y+2); j++) {
			GameObject t = getTile(x, j);
			if (t != null && t.GetComponent<TileState>().isCreep) {
				setTile(null, x, j);
				Destroy(t);
			}
		}
	}

	private bool bordersCreep(int x, int y) {
		// first horizontal
		for (int i = Math.Max(0, x-1); i < Math.Min(size, x+2); i++) {
			GameObject t = getTile(i, y);
			if (t != null && t.GetComponent<TileState>().isCreep) {
				return true;
			}
		}
		
		// then vertical
		for (int j = Math.Max(0, y-1); j < Math.Min(size, y+2); j++) {
			GameObject t = getTile(x, j);
			if (t != null && t.GetComponent<TileState>().isCreep) {
				return true;
			}
		}

		return false;
	}

	private class Edible {
		public GameObject tile;
		public int x, y;

		public Edible(GameObject tile, int x, int y) {
			this.tile = tile;
			this.x = x;
			this.y = y;
		}
	}

	public void doCreepSuper() {
		creepSuperTimer = 2;
		creepDuration = 0;
	}

	private void doExpandCreep(int howMany = 2) {
		List<Edible> tasty = new List<Edible>();
		List<Edible> regular = new List<Edible>();

		for (int i = 0; i < size; ++i) {
			for (int j = 0; j < size; ++j) {
				GameObject t = getTile(i, j);
				if (t != null && !t.GetComponent<TileState>().isCreep && !t.GetComponent<TileState>().isAnimating && bordersCreep(i, j)) {
					if (t.GetComponent<TileState>().isTasty) {
						tasty.Add(new Edible(t, i, j));
					} else {
						regular.Add(new Edible(t, i, j));
					}
				}
			}
		}

		if (tasty.Count == 0 && regular.Count == 0) {
			// No creep on the board. Make sure to spawn an extra 2 creeps this turn
			howMany += 2;
			GetComponent<BoomBox>().onCreepInfest();
		}

		for (int i = 0; i < howMany; ++i) {
			if (tasty.Count > 0 && creepSuperTimer < 0) {
				int which = UnityEngine.Random.Range(0, tasty.Count);
				Edible e = tasty[which];
				tasty.RemoveAt(which);
				spawnCreep(e.x, e.y);
				Destroy(e.tile);
				GetComponent<BoomBox>().onTastyTileConsumed();
				if (gameScore) {
					gameScore.onCreepScoreEvent(400);
				}
			} else if (regular.Count > 0) {
				int which = UnityEngine.Random.Range(0, regular.Count);
				Edible e = regular[which];
				regular.RemoveAt(which);
				spawnCreep(e.x, e.y);
				Destroy(e.tile);
			} else {
				int x = UnityEngine.Random.Range(0, size);
				int y = UnityEngine.Random.Range(0, size);
				GameObject t = getTile(x, y);
				if (t != null && !t.GetComponent<TileState>().isAnimating) {
					spawnCreep(x, y);
					Destroy(t);
				}
			}
		}

		if (didCreepWin()) {
			gameState.OnFail();
		}
	}

	private void spawnCreep(int x, int y) {
		spawnPrefab(creepPrefabs[UnityEngine.Random.Range(0, creepPrefabs.Length)], x, y, false);
	}

	private void spawnPrefab(GameObject prefab, int x, int y, bool animate = true) {
		GameObject t = (GameObject)Instantiate(prefab);
		t.transform.parent = gameObject.transform;
		if (animate) {
			animations.Add(new ScaleAnimation(t, 0, 1, 0.25f, 0.125f));
		}
		setTile(t, x, y);
	}
	
	public static Vector3 getLocalPosition(GameObject t) {
		if (t != null) {
			return t.transform.localPosition - new Vector3(0.5f, 0.5f, 0);
		}
		return Vector3.zero;
	}

	public static void setLocalPosition(GameObject t, float x, float y, float z) {
		if (t != null) {
			t.transform.localPosition = new Vector3(x + 0.5f, y + 0.5f, z);
		}
	}

	private void onScoreEvent(int run, int x, int y) {
		int key = x*size + y;
		if (!accumulateScores.ContainsKey(key)) {
			accumulateScores[key] = 0;
		}

		accumulateScores[key] += (run - 2);
	}

	private void submitScores() {
		if (gameHasStarted) {
			foreach (KeyValuePair<int, int> pair in accumulateScores) {
				int which = Math.Max(0, Math.Min(scorePrefabs.Length-1, pair.Value - 1));

				if (gameScore != null) {
					gameScore.onScoreEvent(scoreLevels[which]);
				}

				int x = pair.Key / size;
				int y = pair.Key % size;

				GameObject t = (GameObject)Instantiate(scorePrefabs[which]);
				t.transform.parent = transform;
				t.transform.localPosition = new Vector3(x, y, -2);
			}
		}
		accumulateScores.Clear();
	}
}
