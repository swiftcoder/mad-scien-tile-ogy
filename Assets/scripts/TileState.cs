﻿using UnityEngine;
using System.Collections;

public class TileState : MonoBehaviour {

	public bool isCreep = false;
	public bool isTasty = false;

	[HideInInspector]
	public bool isAnimating = false;
	
	public bool matches(TileState other) {
		if (other == null)
			return false;

		// Creep never matches
		if (isCreep || other.isCreep)
			return false;

		return name.Equals(other.name);
	}

	public bool uCantTouchThis() {
		return isCreep;
	}
}
