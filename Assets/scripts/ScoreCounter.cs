﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreCounter : MonoBehaviour {

	public float animationDuration = 3.0f;

	private Text text;
	private float currentScore;
	private float finalScore;

	private float startTime = 0f;
	private float endTime = 0f;

	public void setScore(int score, bool animated) {
		finalScore = score;
		currentScore = 0f;

		startTime = Time.time;
		endTime = startTime + animationDuration;

		updateText ();
	}

	void Start () {
		text = GetComponent<Text> ();
	}
	
	void Update () {
		if (Time.time >= startTime && Time.time <= endTime) { 
			currentScore = getNewScore ();
		} else {
			currentScore = finalScore;
		}
		updateText ();
	}

	private int getNewScore() {
		if (Time.time > endTime) {
			return (int)finalScore;
		} else if (Time.time < startTime) {
			return (int)currentScore;
		} else {
			float elapsedTime = Time.time - startTime;
			float percent = elapsedTime / animationDuration;
			return Mathf.CeilToInt(finalScore * percent);
		}
	}

	private void updateText() {
		text.text = string.Format("Score: {0}", currentScore);
	}
}
