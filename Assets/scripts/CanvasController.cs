﻿using UnityEngine;
using System.Collections;


public class CanvasController : MonoBehaviour{

	public virtual void Show(bool animated = true) {
		GetComponent<Canvas> ().enabled = true;
	}

	public virtual void Hide(bool animated = true) {
		GetComponent<Canvas> ().enabled = false;
	}
}
