﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameScore : MonoBehaviour {

	public CreepPowerBar creepPowerBar;
	public PowerBar powerBar;
	public Text text;

	private int score;
	private int creepScore;
	
	void Start() {
		score = 0;
		creepScore = 0;

		updateText();
	}
	
	void Update() {
	}

	public int getScore() {
		return score;
	}

	public int getCreepScore() {
		return creepScore;
	}

	public void onScoreEvent(int addedScore) {
		score += addedScore;
		updateText();

		if (powerBar != null) {
			powerBar.onScoreEvent(score);
		}
	}

	public void onCreepScoreEvent(int addedScore) {
		creepScore += addedScore;

		if (creepPowerBar != null) {
			creepPowerBar.onScoreEvent(creepScore);
		}
	}

	private void updateText() {
		text.text = string.Format("Score: {0}", score);
	}
}
