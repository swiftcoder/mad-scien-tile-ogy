﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class GameTimer : MonoBehaviour {
	
	public float durationInSeconds = 60.0f;

	public GameState gameState;

	private Text text;
	private float remaining;
	private bool live;

	void Start() {
		text = GetComponent<Text>();
		live = false;

		updateText();
	}

	void Update() {
		if (!live)
			return;

		if (remaining > 0) {
			remaining -= Time.deltaTime;
			updateText();
		} else {
			live = false;
			timerFired();
		}
	}

	public void doBegin() {
		remaining = durationInSeconds;
		live = true;
	}

	public void doPause() {
		live = false;
	}

	public void doResume() {
		live = true;
	}

	private void updateText() {
		TimeSpan timeSpan = TimeSpan.FromSeconds(remaining);
		text.text = String.Format("Time: {0,2:00}:{1,2:00}", timeSpan.Minutes, timeSpan.Seconds);
	}

	private void timerFired() {
		print("Hey! You! The timer fired!");
		gameState.OnTimeExpired();
	}
}
