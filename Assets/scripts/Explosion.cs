﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	public float lifetime = 1f;

	void Start() {
	}
	
	void Update () {
		lifetime -= Time.deltaTime;
		if (lifetime <= 0) {
			Destroy(gameObject);
		}
	}
}
