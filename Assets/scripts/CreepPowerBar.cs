﻿using UnityEngine;
using UnityEngine.UI;
using System;

using System.Collections;

public class CreepPowerBar : BaseBar {

	protected override void onMeterDrained() {
		if (gameBoard != null) {
			gameBoard.doCreepSuper();
		}
	}

	protected override void onMeterFull() {
		print("The creep's meter overfloweth with tasty biomass!");
		if (gameBoard != null) {
			gameBoard.GetComponent<BoomBox>().onCreepSuper();
		}
	}
}
