﻿using UnityEngine;
using System.Collections;

public class ScorePopup : MonoBehaviour {

	public float lifetime = 2f;
	public float speed = 1f;

	void Start () {
	}
	
	void Update () {
		Vector3 pos = transform.localPosition;
		pos.y += speed * Time.deltaTime;
		transform.localPosition = pos;

		lifetime -= Time.deltaTime;
		if (lifetime <= 0) {
			Destroy(gameObject);
		}
	}
}
