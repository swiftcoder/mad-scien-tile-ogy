﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public abstract class BaseBar : MonoBehaviour {

	const float GROWTH_RATE = 1.5f;
	const float MAX_THRESHOLD = 7000f;
	
	float threshold = 2500f;
	
	float minScore = 0f;
	float maxScore;
	float score = 0f;
	
	public Image barImage;
	public GameBoard gameBoard;
	
	bool inDrain = false;
	float barPercentage = 0f;

	void Start() {
		maxScore = threshold;
		
		updateBarPercentage(scorePercentage(score));
	}

	void Update() {
		if (inDrain) {
			barPercentage -= Time.deltaTime;
			updateBarDisplay();
			
			if (barPercentage < 0) {
				inDrain = false;
				onMeterDrained();
			}
		}
	}

	float scorePercentage(float amount) {
		return Mathf.Clamp01( (amount - minScore) / (maxScore - minScore) );
	}
	
	void updateBarPercentage(float percent) {
		if (!inDrain) {
			barPercentage = percent;
			updateBarDisplay();
		}
	}
	
	void updateBarDisplay() {
		float height = barImage.rectTransform.rect.height;
		Vector3 position = barImage.transform.localPosition;
		position.y = -height + (barPercentage * height);
		barImage.transform.localPosition = position;
	}

	public void onScoreEvent(int newScore) {
		score = newScore;
		if (score >= maxScore) {
			minScore = maxScore;
			threshold = Math.Min(MAX_THRESHOLD, threshold * GROWTH_RATE);
			maxScore = maxScore + threshold;
			meterFull();
		}
		updateBarPercentage(scorePercentage(score));
	}

	private void meterFull() {
		inDrain = true;
		onMeterFull();
	}

	protected abstract void onMeterDrained();

	protected abstract void onMeterFull();
}
